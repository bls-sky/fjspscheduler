# FJSPScheduler

#### 介绍
用于解决多目标柔性车间调度问题的调度器。
未来将融合强化学习、鲁棒性调度。
#### 声明
开源许可证：GPL2.0

#### 软件架构
maven项目，全java编写。

首先，解决了柔性车间调度问题。柔性车间调度问题分为两子问题：工序加工顺序的选择；工序加工机器的选择。本项目使用了两种方法：基于优先规则的调度；基于遗传算法的调度。

其次，解决了多目标的调度问题。根据三个目标，甚至三个以上目标（综合考虑很多约束条件），去选择出种群中较优的个体作为最终调度方案。本项目目前设计了两个多目标优化的算法：NSGA-II（两个目标），NSGA-III（三个目标及以上）。

Main类：运行类

scheduler包：
- SampleScheduler：基于优先规则的调度类。调度的整体框架
- GAScheduler：遗传算法的调度类。调度的整体框架，通过调用不同函数实现不同算法
- GAScheduleManager：不同类型遗传算法的具体实现（原始遗传算法、多目标遗传算法NSGA-II、NSGA-III）
- GeneticConfiguration：遗传算法的配置类，所有算法用到的参数都保存在这
- ChromosomeOperation：遗传算法的算子，选择、交叉、变异
- NSGAII：NSGAII算法的实现，快速非支配排序、拥挤度计算等
- NSGAIII：NSGAIII算法的实现，目标归一化、参考点选取等

util包：
- FileHandler：将输入的数据转化，存储在项目中的对象里
- InstanceUtil：负责项目的各种输出、打印工作，将种群绘制在2维或3维坐标系中（多个目标的值组成一个点）、绘制文字版本的甘特图、打印配置信息等

pojo包：
- Product 工单产品实体类
- Process 工序实体类
- Machine 机器设备实体类
- Instance 调度案例实体类，一个对象存储一个调度案例。包含工单、工序、设备、工序比较器、设备比较器、调度目标等
- Chromosome 染色体实体类，染色体的工序编码、设备选择编码、适应度值、NSGA算法相关染色体的参数
- CandidateMachine 候选设备类，某一工单的某一工序可以选择运行的设备，总工时、准备时间、加工时间等

rule包：
- machinerule包：某一道工序选择设备时，有多个设备可以选择，在这个包下定义设备选择的规则，按照规则选择设备
- processrule包：排班的时候要一个一个的排工序，所以要按照一定的规则给所有工单的工序进行排序，按优先顺序进行调度。（我的理解遗传算法也是找出工序的一个最优的调度顺序）
objective包：不同的调度目标类，每一个instance实例都有若干个目标来表示其调度结果的好坏。染色体中也要根据目标的值计算适应度值，然后遗传算法根据适应度值淘汰不好的个体。

manager包：主要是两个类
- SchedulerManager：调度的具体实现，也是基于优先规则的调度算法的具体实现。包括调度前的准备工作，调度后的处理工作，还有调用ProcessManag类实现工序分配。
- ProcessManager：工序分配的具体实现，选择工序，分配设备，并使用插空的方法把设备的空闲时间分配给工序。


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
