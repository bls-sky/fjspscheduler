package com.aaaa.scheduler;

import com.aaaa.scheduler.pojo.*;
import com.aaaa.scheduler.rule.processrule.ProcessEDD;
import com.aaaa.scheduler.scheduler.GAScheduler;
import com.aaaa.scheduler.scheduler.SampleScheduler;
import com.aaaa.scheduler.util.FileHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main {
    public static void main(String[] args) throws Exception {
        //读取样例
//        Instance instance = FileHandler.readJSP("jsp.txt");
        Instance instance = FileHandler.readFJSP("fjsp02.txt");
        //开始调度
        instance.setWirteDynamic(true);
        SampleScheduler sampleScheduler = new SampleScheduler(new ProcessEDD());
        sampleScheduler.schedule(instance, 0);
//        GAScheduler gaScheduler = new GAScheduler();
//        gaScheduler.schedule(instance, 0);
    }
}
//给定一组非负整数 nums，重新排列每个数的顺序（每个数不可拆分）使之组成一个最大的整数。
//注意：输出结果可能非常大，所以你需要返回一个字符串而不是整数

/*工单：交货期越早优先  设备：加工时间最短优先
工单：2  工序：1  开始时间：0.0  结束时间：1.0  设备：3
当前的制造期(Cmax):1.0
工单：1  工序：1  开始时间：0.0  结束时间：4.0  设备：2
当前的制造期(Cmax):4.0
工单：3  工序：1  开始时间：4.0  结束时间：8.0  设备：2
当前的制造期(Cmax):8.0
工单：3  工序：2  开始时间：8.0  结束时间：10.0  设备：1
当前的制造期(Cmax):10.0
工单：1  工序：2  开始时间：8.0  结束时间：10.0  设备：2
当前的制造期(Cmax):10.0
工单：2  工序：2  开始时间：1.0  结束时间：4.0  设备：1
当前的制造期(Cmax):10.0
**********************EDD规则:工件交货期越早越优先**********************
制造期(Cmax):10.0
设备3的负荷:0.1
设备2的负荷:1.0
设备1的负荷:0.5
**********************甘特图**********************
   time:  1  2  3  4  5  6  7  8  9 10
machine:     2  2  2              3  3
machine:  1  1  1  1  3  3  3  3  1  1
machine:  2
调度用时:0.007秒
 */

/*工单：交货期越早优先  设备：加工时间最短优先
工单：2  工序：1  开始时间：0.0  结束时间：2.0  设备：1
当前的制造期(Cmax):2.0
工单：1  工序：1  开始时间：0.0  结束时间：4.0  设备：2
当前的制造期(Cmax):4.0
工单：3  工序：1  开始时间：0.0  结束时间：5.0  设备：3
当前的制造期(Cmax):5.0
工单：3  工序：2  开始时间：5.0  结束时间：7.0  设备：1
当前的制造期(Cmax):7.0
工单：1  工序：2  开始时间：7.0  结束时间：11.0  设备：1
当前的制造期(Cmax):11.0
工单：2  工序：2  开始时间：4.0  结束时间：8.0  设备：2
当前的制造期(Cmax):11.0
**********************EDD规则:工件交货期越早越优先**********************
制造期(Cmax):11.0
设备3的负荷:0.45454545454545453
设备2的负荷:0.7272727272727273
设备1的负荷:0.7272727272727273
**********************甘特图**********************
   time:  1  2  3  4  5  6  7  8  9 10 11
machine:  2  2           3  3  1  1  1  1
machine:  1  1  1  1  2  2  2  2
machine:  3  3  3  3  3
调度用时:0.006秒
 */