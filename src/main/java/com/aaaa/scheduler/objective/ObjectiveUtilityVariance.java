package com.aaaa.scheduler.objective;

import com.aaaa.scheduler.pojo.Instance;
import com.aaaa.scheduler.pojo.Machine;
import com.aaaa.scheduler.pojo.Process;
import com.aaaa.scheduler.util.InstanceUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述：所有设备利用率的方差
 *
 */
public class ObjectiveUtilityVariance extends Objective{

    public ObjectiveUtilityVariance() {
        super();
        // TODO Auto-generated constructor stub
        this.objectiveName = "所有设备利用率的方差";
    }

    @Override
    public double calcValue(Instance instance) {
        // TODO Auto-generated method stub
        lastObjectiveValue = objectiveValue;
        List<Double> utilities = new ArrayList<>();
        // 制造期
        double cmax = InstanceUtil.calcCmax(instance);
        // 利用率计算
        for (Machine machine : instance.getMachineMap().values()) {
            double util = 0;
            for (Process process : machine.getQueueList()) {
                util += process.getPreTime() + process.getRunTime() * process.getProduct().getPlanNum();
            }
            utilities.add(util / cmax);
        }
        double sum = 0, avg = 0, variance = 0;
        for (Double utility : utilities) {
            sum += utility;
        }
        avg = sum/utilities.size();
        for (Double utility : utilities) {
            variance += Math.pow(utility-avg, 2);
        }
        variance /= utilities.size();

        this.objectiveValue = variance;
        return variance;
    }
}
