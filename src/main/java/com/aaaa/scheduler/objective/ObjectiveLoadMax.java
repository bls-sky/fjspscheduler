package com.aaaa.scheduler.objective;

import com.aaaa.scheduler.pojo.Instance;
import com.aaaa.scheduler.pojo.Machine;
import com.aaaa.scheduler.pojo.Process;

/**
 * 功能描述：所有设备中负荷最大的那个
 *
 */
public class ObjectiveLoadMax extends Objective{

    public ObjectiveLoadMax() {
        super();
        // TODO Auto-generated constructor stub
        this.objectiveName = "所有设备中最大的负荷";
    }

    @Override
    public double calcValue(Instance instance) {
        // TODO Auto-generated method stub
        lastObjectiveValue = objectiveValue;
        // 总负荷
        double loadMax = 0;
        for (Machine machine : instance.getMachineMap().values()) {
            double load = 0;
            for (Process process : machine.getQueueList()) {
                load += process.getRunTime() * process.getProduct().getPlanNum();
            }
            loadMax = Math.max(loadMax, load);
        }

        this.objectiveValue = loadMax;
        return loadMax;
    }
}
