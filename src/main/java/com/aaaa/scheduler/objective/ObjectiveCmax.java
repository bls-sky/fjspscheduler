package com.aaaa.scheduler.objective;

import com.aaaa.scheduler.pojo.Instance;
import com.aaaa.scheduler.pojo.Machine;
import com.aaaa.scheduler.pojo.Process;

/**
 * 功能描述：最大完成时间
 *
 */
public class ObjectiveCmax extends Objective{

    public ObjectiveCmax() {
        super();
        // TODO Auto-generated constructor stub
        this.objectiveName = "制造期(Cmax)";
    }

    @Override
    public double calcValue(Instance instance) {
        // TODO Auto-generated method stub
        lastObjectiveValue = objectiveValue;
        double value = 0;
//        for (Process process : instance.getProcessMap().values()) {
//            if (process.getFinish() > value) {
//                value = process.getFinish();
//            }
//        }
        for (Machine machine : instance.getMachineMap().values()) {
            for (Process process : machine.getQueueList()) {
                if(process.getFinish() > value)
                    value = process.getFinish();
            }
        }
        this.objectiveValue = value;
        return value;
    }
}
