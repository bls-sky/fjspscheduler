package com.aaaa.scheduler.objective;

import com.aaaa.scheduler.pojo.Instance;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 调度目标基类
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Objective implements Serializable {
    /**
     * 目标名称
     */
    protected String objectiveName;
    /**
     * 目标值
     */
    protected double objectiveValue;
    /**
     * 上次目标值（用于单步调度）
     */
    protected double lastObjectiveValue;

    /**
     *
     * 计算目标值
     *
     */
    public double calcValue(Instance instance) {
        return this.objectiveValue;
    }
}
