package com.aaaa.scheduler.objective;

import com.aaaa.scheduler.pojo.Instance;
import com.aaaa.scheduler.pojo.Machine;
import com.aaaa.scheduler.pojo.Process;
import com.aaaa.scheduler.util.InstanceUtil;

/**
 * 功能描述：设备总负荷
 *
 */
public class ObjectiveLoadSum extends Objective{

    public ObjectiveLoadSum() {
        super();
        // TODO Auto-generated constructor stub
        this.objectiveName = "所有设备总负荷";
    }

    @Override
    public double calcValue(Instance instance) {
        // TODO Auto-generated method stub
        lastObjectiveValue = objectiveValue;
        // 总负荷
        double loadSum = 0;
        for (Machine machine : instance.getMachineMap().values()) {
            for (Process process : machine.getQueueList()) {
                loadSum += process.getRunTime() * process.getProduct().getPlanNum();
            }
        }

        this.objectiveValue = loadSum;
        return loadSum;
    }
}
