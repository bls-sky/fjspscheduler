package com.aaaa.scheduler.objective;

import com.aaaa.scheduler.pojo.Instance;
import com.aaaa.scheduler.pojo.Machine;
import com.aaaa.scheduler.pojo.Process;
import com.aaaa.scheduler.util.InstanceUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述：所有设备利用率的平均值 的倒数
 *
 */
public class ObjectiveUtilityAvg extends Objective{

    public ObjectiveUtilityAvg() {
        super();
        // TODO Auto-generated constructor stub
        this.objectiveName = "所有设备利用率的平均值 的倒数";
    }

    @Override
    public double calcValue(Instance instance) {
        // TODO Auto-generated method stub
        lastObjectiveValue = objectiveValue;
        // 制造期
        double cmax = InstanceUtil.calcCmax(instance);
        // 利用率计算
        double avg = 0;
        for (Machine machine : instance.getMachineMap().values()) {
            double util = 0;
            for (Process process : machine.getQueueList()) {
                util += process.getPreTime() + process.getRunTime() * process.getProduct().getPlanNum();
            }
            avg += util / cmax;
        }
        avg /= instance.getMachineMap().size();

        this.objectiveValue = 1/avg;
        return 1/avg;
    }
}
