package com.aaaa.scheduler.objective;

import com.aaaa.scheduler.pojo.Instance;
import com.aaaa.scheduler.pojo.Machine;
import com.aaaa.scheduler.pojo.Process;
import com.aaaa.scheduler.pojo.Product;

/**
 * 功能描述：最大完成时间
 *
 */
public class ObjectiveDelivery extends Objective{

    public ObjectiveDelivery() {
        super();
        // TODO Auto-generated constructor stub
        this.objectiveName = "超交付期时间总和 只统计超期的工单";
    }

    @Override
    public double calcValue(Instance instance) {
        // TODO Auto-generated method stub
        lastObjectiveValue = objectiveValue;
        double value = 0;
        for (Product product : instance.getProductMap().values()) {
            if(product.getFinish() > product.getDueDate()){
                value += product.getFinish() - product.getDueDate();
            }
        }
        this.objectiveValue = value;
        return value;
    }
}
