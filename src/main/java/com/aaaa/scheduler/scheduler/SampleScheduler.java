package com.aaaa.scheduler.scheduler;

import com.aaaa.scheduler.manager.impl.SchedulerManager;
import com.aaaa.scheduler.pojo.Instance;
import com.aaaa.scheduler.rule.processrule.ProcessRule;
import com.aaaa.scheduler.util.InstanceUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class SampleScheduler {
    private SchedulerManager schedulerManager = new SchedulerManager();
    /**
     * 是否执行多规则调度。默认不执行
     */
    private boolean multiRules = false;
    /**
     * 规则
     */
    private ProcessRule processRule;
    /**
     * 规则编号
     */
    private int ruleID;

    public SampleScheduler(ProcessRule processRule) {
        super();
        this.processRule = processRule;
        multiRules = false;
    }
    public SampleScheduler(int ruleID) {
        super();
        this.ruleID = ruleID;
        multiRules = false;
    }

    /**
     * 功能描述：执行调度
     *
     */
    public void schedule(Instance instance, int mode) throws Exception {
        long lStart = System.currentTimeMillis();
        try {
            // 设置比较器
            if (multiRules) {
                InstanceUtil.setProcessComparator(instance, this.ruleID);
            } else {
                InstanceUtil.setProcessComparator(instance, processRule);
            }
            // 调度前处理
            schedulerManager.beforeSchedule(instance, mode);
            // 设置算法名称
            instance.setAltorighmName(instance.getProcessComparator().getRuleName());
            // 调度
            schedulerManager.schedule(instance, mode);
            // 调度后处理
            schedulerManager.afterSchedule(instance, mode);
            // 打印结果
            InstanceUtil.printResultSampleScheduler(instance);
            InstanceUtil.printGante(instance);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        long lEnd = System.currentTimeMillis();
        double diff = (lEnd - lStart) / 1000.0;// 得到两者的秒数
        System.out.println("调度用时:" + diff + "秒");
    }
}
