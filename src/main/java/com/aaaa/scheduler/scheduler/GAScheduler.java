package com.aaaa.scheduler.scheduler;

import com.aaaa.scheduler.pojo.Instance;
import com.aaaa.scheduler.scheduler.genetic.GAScheduleManager;
import com.aaaa.scheduler.util.InstanceUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class GAScheduler {
    private GAScheduleManager gaScheduleManager = new GAScheduleManager();

    /**
     * 功能描述：执行调度
     *
     */
    public void schedule(Instance instance, int mode) throws Exception {
        long lStart = System.currentTimeMillis();
        try {
            // 调度前处理
            gaScheduleManager.beforeSchedule(instance, mode);
            // 设置算法名称
            instance.setAltorighmName("遗传算法");
            // 调度
            gaScheduleManager.nsga2Schedule(instance, mode);
            // 调度后处理
            gaScheduleManager.afterSchedule(instance, mode);
            // 打印结果
            InstanceUtil.printResultGAScheduler(instance);
            //打印甘特图
            InstanceUtil.printGante(instance);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        long lEnd = System.currentTimeMillis();
        double diff = (lEnd - lStart) / 1000.0;// 得到两者的秒数
        System.out.println("调度用时:" + diff + "秒");
    }
}
