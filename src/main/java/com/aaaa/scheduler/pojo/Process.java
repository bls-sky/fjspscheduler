package com.aaaa.scheduler.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Process implements Serializable {
    /**
     * 已分派状态
     */
    public static final int Assigned_State = 1;
    /**
     * 未分派状态
     */
    public static final int Unassigned_state = 0;
    /**
     * 工序号
     */
    private int ID;
    /**
     * 工序名称
     */
    private String name;
    /**
     * 工件号
     */
    private int productID;
    /**
     * 工件
     */
    private Product product;
    /**
     * 工序顺序（排序用）
     */
    private int seq;
    /**
     * 准备时间
     */
    private double preTime;
    /**
     * 单件时间
     */
    private double runTime; // 选择工时
    /**
     * 设备编号
     */
    private int machineID; // 选择设备
    /**
     * 设备名称
     */
    private String machineName; // 选择设备
    /**
     * 固定的机床编号，用于解码时确定已安排的机床(-1表示未被安排机床)
     */
    private int fixedMachineID = -1;
    /**
     * 可选设备数量
     */
    private int candidateProNum; // 可选设备数量
    /**
     * 最早开始
     */
    private double earlyStart;
    /**
     * 开始
     */
    private double start;
    /**
     * 结束
     */
    private double finish;
    /**
     * 计划数，默认为1
     */
//    private int planQty = 1;
    /**
     * 前置工序
     */
    private Process prepOp;
    /**
     * 后置工序
     */
    private Process SuccOp;
    /**
     * 状态0未调度 1已调度
     */
    private int state;
    /**
     * 剩余工序数
     */
    private int remainProcessNum;


    /**
     * 权重
     */
    private double weight;// 权重
    /**
     * 交货期
     */
    private int dueDate;
    /**
     * 剩余工时
     */
    private double remainWorkTime;

    /**
     * 工序优先级
     */
    private double opPriority;

    public Process(int kk, String name, int productID, Product product) {
        this.ID = kk;
        this.name = name;
        this.productID = productID;
        this.product = product;
    }

    /**
     * 临时使用，因为可能对于不同的设备工时不同
     *
     * @return 下午3:42:57
     */
    public double getWorkTime() {
        return this.product.getPlanNum() * this.runTime + this.preTime;
    }
}
