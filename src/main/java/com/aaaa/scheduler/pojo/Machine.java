package com.aaaa.scheduler.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Comparator;
import java.util.TreeSet;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Machine implements Serializable{

    private int ID;
    private String name;
    /**
     * 机床已安排的工时
     */
    private double assignedTaskWork;
    /**
     * 利用率
     */
    private double utilRation;
    /**
     * 负荷率
     */
    private double loadRation;

    public Machine(int i, String name) {
        this.ID = i;
        this.name = name;
    }

    class AssginedOpComp implements Comparator<Process>, Serializable {
        @Override
        public int compare(Process o1, Process o2) {
            // TODO Auto-generated method stub
            if (o1.getStart() > o2.getStart()) {
                return 1;
            } else if (o1.getStart() < o2.getStart()) {
                return -1;
            } else {
                return 0;
            }
        }
    }
    // 队列结果
    private TreeSet<Process> queueList = new TreeSet<>(new AssginedOpComp());
}
