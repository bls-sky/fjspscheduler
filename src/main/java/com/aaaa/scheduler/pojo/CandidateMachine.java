package com.aaaa.scheduler.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CandidateMachine implements Serializable {

    /**
     * 生产所有件数所需的总工时
     */
    private double duration;

    /**
     * 准备时间
     */
    private double setupTime;

    /**
     * 单件加工时间
     */
    private double runTime;

    private Machine machine;

}
