package com.aaaa.scheduler.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {
    /**
     * 零件号
     */
    private int ID;
    /**
     * 零件名称
     */
    private String name;
    /**
     * 工序总数
     */
    private int operationNum; // 工序数量
    /**
     * 工序列表
     */
    private List<Process> opList; // 工艺路线
    /**
     * 计划开始
     */
    private double start;
    /**
     * 计划结束
     */
    private double finish;
    /**
     * 权重
     */
    private double weight;// 权重
    /**
     * 交货期
     */
    private double dueDate;// 交货期
    /**
     * 计划数，默认为1
     */
    private int planNum = 1;
    /**
     * 当前工序
     */
    private Process currOp;
    /**
     * 总工时
     */
    private double totalWorkTime;


    public Product(int ID, String name, int planNum) {
        this.ID = ID;
        this.name = name;
        this.planNum = planNum;
    }

    //TODO 考虑工单的交货期
    public Product(int ID, String name, int planNum, double dueDate) {
        this.ID = ID;
        this.name = name;
        this.planNum = planNum;
        this.dueDate = dueDate;
    }
}
