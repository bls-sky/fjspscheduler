package com.aaaa.scheduler;

/**
 * 功能描述：测试绘制三维散点图
 *
 */


import java.util.ArrayList;
import java.util.List;

import tanling.matplot3d.app.facade.DotsDataProcessor;
import tanling.matplot3d.app.facade.Matplot3D4JMgr;
import tanling.matplot3d.common.Point3D;

public class Test3 {

    public static void main(String[] args) throws Exception {

        DotsDataProcessor processor = new DotsDataProcessor();

        final Matplot3D4JMgr mgr=new Matplot3D4JMgr(processor);

        //*************************************************************//
        //在此准备数据，将Point3D对象放入List<Point3D>容器中
        //prepare your data here

        List<Point3D> dos1=new ArrayList<Point3D>();
        dos1.add(new Point3D(0,1,1));
        dos1.add(new Point3D(1,2,1));

        //将数据加入处理器（可加入多组）
        processor.addData("Item 1", dos1);

        List<Point3D> dos2=new ArrayList<Point3D>();
        dos2.add(new Point3D(1,1.3,1));
        dos2.add(new Point3D(1,1.6,1));

        processor.addData("Item 2", dos2);
        //.................
        //*************************************************************//

        mgr.setSeeta(0.6);
        mgr.setBeita(1.0);

        mgr.setTitle("自定义标题");

        //坐标参考平面不会遮挡数据
        mgr. setCoordianteSysShowType( Matplot3D4JMgr.COORDINATE_SYS_ALWAYS_FURTHER);

        mgr.show();
    }

}
