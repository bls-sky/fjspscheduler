package com.aaaa.scheduler.manager;

import com.aaaa.scheduler.pojo.CandidateMachine;
import com.aaaa.scheduler.pojo.Instance;
import com.aaaa.scheduler.pojo.Machine;
import com.aaaa.scheduler.pojo.Process;

public interface IProcessManager {
    /**
     *
     * @description: 工序分派
     *
     */
    public void assignTask(Instance instance, Process process, Boolean insert, Boolean isGA, CandidateMachine alreadyMachine) throws Exception;

    /**
     *
     * @description: 工序分派后处理
     *
     */
    public void afterAssign(Instance instance, Process process, CandidateMachine candidateMachine, Boolean isGA) throws Exception;

    /**
     *
     * @description: 从就绪任务集合中移除已调度工序
     *
     */
    public void removeFromReadyTasks(Process process);
}
