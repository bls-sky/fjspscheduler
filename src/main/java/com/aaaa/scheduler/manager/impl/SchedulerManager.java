package com.aaaa.scheduler.manager.impl;

import com.aaaa.scheduler.manager.ISchedulerManager;
import com.aaaa.scheduler.objective.Objective;
import com.aaaa.scheduler.pojo.*;
import com.aaaa.scheduler.pojo.Process;
import com.aaaa.scheduler.rule.processrule.ProcessRule;
import com.aaaa.scheduler.rule.processrule.ProcessSPT;
import com.aaaa.scheduler.util.InstanceUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SchedulerManager implements ISchedulerManager {

    protected ProcessManager processManager = new ProcessManager();

    @Override
    public void doSchedule(Instance instance, int mode) throws Exception {

    }
    @Override
    public void beforeSchedule(Instance instance, int mode) throws Exception {
        instance.init();
        instance.reset();
    }

    @Override
    public void schedule(Instance instance, int mode) throws Exception {
        this.initReadyTasks(instance);
        // 只要就绪任务集合不为空，则继续安排
        while (!instance.getReadyTaskS().isEmpty()) {
            // 获得最优先的任务
            Process process = instance.getReadyTaskS().first();
            // 分派任务
            processManager.assignTask(instance, process, true, false, null);
            // 用于保存每步分派结果，每一步的instance都是当前的状态
            if (instance.isWirteDynamic()) {
                InstanceUtil.outputSolutionStep(process);
            }
        }
    }


    @Override
    public void afterSchedule(Instance instance, int mode) throws Exception {
        // 设置订单的开始和结束
        for (Product product : instance.getProductMap().values()) {
            if (product.getOpList().size() > 0) {
                int opNum = product.getOpList().size();
                product.setStart(product.getOpList().get(0).getStart());
                product.setFinish(product.getOpList().get(opNum - 1).getFinish());
            }
        }
        // 设置每台设备的利用率和负荷
        InstanceUtil.setMachineUtilityAndLoad(instance);

        //计算所有目标的值
//        instance.getObjective().calcValue(instance);
        for (Objective objective : instance.getObjectiveList()) {
            objective.calcValue(instance);
        }
    }


    @Override
    public void initReadyTasks(Instance instance) throws Exception {
        Comparator<Process> operationTaskComparator = instance.getProcessComparator();
        if (operationTaskComparator == null)
            operationTaskComparator = new ProcessSPT();
        // 设置工序优先级比较器
        TreeSet<Process> readyTaskS = new TreeSet<>(operationTaskComparator);
        for (Product product : instance.getProductMap().values()) {
            product.setCurrOp(product.getOpList().get(0));//把每个订单的第一道工序加入就序列表
            readyTaskS.add(product.getCurrOp());
        }
        instance.setReadyTaskS(readyTaskS);
    }



    @Override
    public void clearScheduleResult(Instance instance) throws Exception {

    }

    @Override
    public Instance cloneInstance(Instance instance) throws Exception {
        return null;
    }

    @Override
    public void calEarlyStart(Instance instance, Process process) throws Exception {

    }

    @Override
    public double scheduleStep(Instance instance, Integer action) {
        return 0;
    }

    @Override
    public double scheduleStep(Instance instance, ProcessRule processRule) {
        return 0;
    }
}
