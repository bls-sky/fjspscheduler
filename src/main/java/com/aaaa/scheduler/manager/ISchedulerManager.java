package com.aaaa.scheduler.manager;

import com.aaaa.scheduler.pojo.Chromosome;
import com.aaaa.scheduler.pojo.Instance;
import com.aaaa.scheduler.pojo.Process;
import com.aaaa.scheduler.rule.processrule.ProcessRule;

public interface ISchedulerManager {
    /**
     *
     * @description:调度入口，进行完整的调度过程(调度前处理，执行调度，调度后处理)
     *  mode 模式（针对具有学习功能的算法，通过该值可以确定是离线训练还是在线应用）
     *
     */
    public void doSchedule(Instance instance, int mode) throws Exception;

    /**
     *
     * @description: 按照不同的模式进行调度
     *  mode 模式（针对具有学习功能的算法，通过该值可以确定是离线训练还是在线应用）
     *
     */
    public void schedule(Instance instance, int mode) throws Exception;


    /**
     *
     * @description: 清除调度结果
     *
     */
    public void clearScheduleResult(Instance instance) throws Exception;

    /**
     *
     * @description: 深度克隆出一个新scheme,主要用于计算
     *
     */
    public Instance cloneInstance(Instance instance) throws Exception;

    /**
     *
     * @description: 调度前处理
     *
     */
    public void beforeSchedule(Instance instance, int mode) throws Exception;

    /**
     *
     * @description: 调度后处理
     *
     */
    public void afterSchedule(Instance instance, int mode) throws Exception;

    /**
     *
     * @description:调度就绪任务初始化
     *
     */
    public void initReadyTasks(Instance instance) throws Exception;

    /**
     *
     * @description: 计算工序最早开始
     *
     */
    public void calEarlyStart(Instance instance, Process process) throws Exception;

    /**
     *
     * @description: 单步调度执行(规则编号)
     *
     */
    public double scheduleStep(Instance instance, Integer action);

    /**
     * @description: 单步调度执行(规则)
     *
     */
    public double scheduleStep(Instance instance, ProcessRule processRule);


}
