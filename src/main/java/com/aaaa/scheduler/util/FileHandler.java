package com.aaaa.scheduler.util;

import com.aaaa.scheduler.pojo.CandidateMachine;
import com.aaaa.scheduler.pojo.Instance;
import com.aaaa.scheduler.pojo.Machine;
import com.aaaa.scheduler.pojo.Process;
import com.aaaa.scheduler.pojo.Product;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class FileHandler {

    /**
     * 功能描述：所有工单的id，对应工单生产数量，工单的交货期，所有产线的id，对应工单可选的产线id，该产线生产一个该工单的时间
     * TODO 规定每个工单最小的拆分数量 比如1000拆分成10个100，这样就将一个工单拆分成十个工单
     */
    public static Instance readOneProductDataFJSP(List<String> productIDs, List<Integer> productNums, List<Double> dueDatas,
                                                  List<String> machineIDs,
                                                  List<List<String>> candidateMachineIDList,
                                                  List<List<Double>> candidateMachineTimeList){
        Instance instance = new Instance();
        Map<String, Product> productMap = new HashMap<>();//所有订单
        Map<String, Machine> machineMap = new HashMap<>();//所有生产设备
        Map<String, List<CandidateMachine>> candidateMachineMap = new HashMap<>();//工序的候选生产设备
        List<CandidateMachine> candidateMachineList;
        List<Integer> canMachineNumList = new ArrayList<Integer>();

        instance.setMachineNum(machineIDs.size());//总的设备数量
        for (int i = 0; i < machineIDs.size(); i++){//初始化所有设备
            machineMap.put(machineIDs.get(i),new Machine(i, machineIDs.get(i)));
        }
        instance.setTotalProductNum(productIDs.size());//总的工单数量
        List<Integer>[] processToIndex = new List[productIDs.size()];
        for (int i = 0; i < productIDs.size(); i++) {//初始化所有订单
            String productID = productIDs.get(i);
            productMap.put(productID, new Product(i+1, productID, productNums.get(i), dueDatas.get(i)));
            processToIndex[i] = new ArrayList<>();
        }
        instance.setProcessToIndex(processToIndex);

        for (int i = 0; i < productIDs.size(); i++) {//设置每个工单的工序
            Product product = productMap.get(productIDs.get(i));
            product.setOperationNum(1);
            List<Process> oplist = new ArrayList<>();

            //创建一道工序
            Process process = new Process(0, productIDs.get(i)+"-process:0", product.getID(), product);
            oplist.add(process);
            instance.getProcessToIndex()[i].add(i);
            //设备可选产线
            int canMachineNum = candidateMachineIDList.get(i).size();
            candidateMachineList = candidateMachineMap.getOrDefault(process.getName(), new ArrayList<>());
            for (int i1 = 0; i1 < canMachineNum; i1++) {
                String mID = candidateMachineIDList.get(i).get(i1);
                double time = candidateMachineTimeList.get(i).get(i1);
                CandidateMachine candidateMachine =
                        new CandidateMachine(0+time*process.getProduct().getPlanNum(), 0, time, machineMap.get(mID));
                candidateMachineList.add(candidateMachine);
            }
            candidateMachineMap.put(process.getName(), candidateMachineList);
            process.setCandidateProNum(canMachineNum);
            canMachineNumList.add(canMachineNum);

            product.setOpList(oplist);//将工序信息保存在订单的数据结构中
        }

        instance.setProductMap(productMap);
        instance.setMachineMap(machineMap);
        instance.setCandidateMachineMap(candidateMachineMap);
        instance.setCanMachineNumList(canMachineNumList);
        return instance;
    }

    public static Instance readJSP(String filename) throws IOException {
        Instance instance = new Instance();
        Map<String, Product> productMap = new HashMap<>();//所有订单
//        Map<String, Process> processMap = new HashMap<>();
        Map<String, Machine> machineMap = new HashMap<>();//所有生产设备
        Map<String, List<CandidateMachine>> candidateMachineMap = new HashMap<>();//工序的候选生产设备
        List<CandidateMachine> candidateMachineList;
        List<Integer> canMachineNumList = new ArrayList<Integer>();

        FileReader fileReader = new FileReader("D:\\Java\\"+filename);
        BufferedReader bufferedReader =new BufferedReader(fileReader);
        String buf = "";
        boolean firstflag = true;
        int productIndex = 1, processIndex = 0;
        while((buf = bufferedReader.readLine()) != null){
            System.out.println(buf);
            if(firstflag){//第一行：订单数、设备数
                String[] r = buf.split("\\s+");
                int productNum = Integer.parseInt(r[0]), machineNum = Integer.parseInt(r[1]);
                instance.setTotalProductNum(productNum);
                instance.setMachineNum(machineNum);
                List<Integer>[] processToIndex = new List[productNum];
                for(int i=1; i<=productNum; i++){//初始化所有订单
                    productMap.put("product:"+i,new Product(i,"product:"+i,1));
                    processToIndex[i-1] = new ArrayList<>();
                }
                instance.setProcessToIndex(processToIndex);
                for(int i=1; i<=machineNum; i++){//初始化所有设备
                    machineMap.put("machine:"+i,new Machine(i,"machine:"+i));
                }
                firstflag = false;
            }else{//其他行：订单的每道工序和生产时间
                String[] r = buf.split("\\s+");
                int productProcessNum = Integer.parseInt(r[0]);//当前订单的工序数
                Product product = productMap.get("product:"+productIndex);
                product.setOperationNum(productProcessNum);
                List<Process> oplist = new ArrayList<>();
                for(int i=1, kk=1; i<r.length; kk++){
                    int machinen = Integer.parseInt(r[i++]), time = Integer.parseInt(r[i++]);
                    //创建一道工序
                    Process process = new Process(kk,product.getName()+"&"+"process:"+kk,product.getID(),product);
                    oplist.add(process);
                    instance.getProcessToIndex()[productIndex-1].add(processIndex++);//添加上工件工序的index索引，定位机器选择染色体
                    //为工序构建可生产的设备
                    CandidateMachine candidateMachine = new CandidateMachine(time,0, time,
                            machineMap.get("machine:"+machinen));
                    candidateMachineList = candidateMachineMap.getOrDefault(process.getName(), new ArrayList<>());
                    candidateMachineList.add(candidateMachine);
                    candidateMachineMap.put(process.getName(),candidateMachineList);
                    canMachineNumList.add(1);
                }
//                System.out.println("oplist:"+oplist);
                product.setOpList(oplist);//将工序信息保存在订单的数据结构中
                productIndex++;
            }
            //可添加固定的机床编号：在排程之前就确定用那台机器生产，设置每道工序的FixedMachineID
        }


        instance.setProductMap(productMap);
        instance.setMachineMap(machineMap);
        instance.setCandidateMachineMap(candidateMachineMap);
        return instance;
    }

    public static Instance readFJSP(String filename) throws IOException {
        Instance instance = new Instance();
        Map<String, Product> productMap = new HashMap<>();//所有订单
        Map<String, Machine> machineMap = new HashMap<>();//所有生产设备
        Map<String, List<CandidateMachine>> candidateMachineMap = new HashMap<>();//工序的候选生产设备
        List<CandidateMachine> candidateMachineList;
        List<Integer> canMachineNumList = new ArrayList<Integer>();

        FileReader fileReader = new FileReader("D:\\Java\\"+filename);
        BufferedReader bufferedReader =new BufferedReader(fileReader);
        String buf = "";
        boolean firstflag = true;
        int productIndex = 1, processIndex = 0;
        while((buf = bufferedReader.readLine()) != null){
            System.out.println(buf);
            if(firstflag){//第一行：订单数、设备数
                String[] r = buf.split("\\s+");
                int productNum = Integer.parseInt(r[0]), machineNum = Integer.parseInt(r[1]);
                instance.setTotalProductNum(productNum);
                instance.setMachineNum(machineNum);
                List<Integer>[] processToIndex = new List[productNum];
                for(int i=1; i<=productNum; i++){//初始化所有订单
                    productMap.put("product:"+i,new Product(i,"product:"+i,1));
                    processToIndex[i-1] = new ArrayList<>();
                }
                instance.setProcessToIndex(processToIndex);
                for(int i=1; i<=machineNum; i++){//初始化所有设备
                    machineMap.put("machine:"+i,new Machine(i,"machine:"+i));
                }
                firstflag = false;
            }else{//其他行：订单的每道工序和生产时间
                String[] r = buf.trim().split("\\s+");
                int productProcessNum = Integer.parseInt(r[0]);//当前订单的工序数
                Product product = productMap.get("product:"+productIndex);
                product.setOperationNum(productProcessNum);
                List<Process> oplist = new ArrayList<>();
                for(int i=1, kk=1; i<r.length; kk++){
                    //创建一道工序
                    Process process = new Process(kk,product.getName()+"&"+"process:"+kk,product.getID(),product);
                    oplist.add(process);
                    instance.getProcessToIndex()[productIndex-1].add(processIndex++);//添加上工件工序的index索引，定位机器选择染色体
                    int canMachineNum = Integer.parseInt(r[i++]);
                    candidateMachineList = candidateMachineMap.getOrDefault(process.getName(), new ArrayList<>());
                    for(int j=0; j<canMachineNum; j++){
                        int machinen = Integer.parseInt(r[i++]), time = Integer.parseInt(r[i++]);
                        //为工序构建可生产的设备
                        CandidateMachine candidateMachine = new CandidateMachine(time,0, time,
                                machineMap.get("machine:"+machinen));
                        candidateMachineList.add(candidateMachine);
                    }
                    candidateMachineMap.put(process.getName(),candidateMachineList);
                    process.setCandidateProNum(canMachineNum);
                    canMachineNumList.add(canMachineNum);
                }
                product.setOpList(oplist);//将工序信息保存在订单的数据结构中
                productIndex++;
            }
            //可添加固定的机床编号：在排程之前就确定用那台机器生产，设置每道工序的FixedMachineID
        }

        instance.setProductMap(productMap);
        instance.setMachineMap(machineMap);
        instance.setCandidateMachineMap(candidateMachineMap);
        instance.setCanMachineNumList(canMachineNumList);
        return instance;
    }
}
