package com.aaaa.scheduler.rule.processrule;

import com.aaaa.scheduler.pojo.Process;

import java.util.Comparator;

/**
 * EDD规则，工件交货期越早越优先
 *
 */
public class ProcessEDD extends ProcessRule implements Comparator<Process> {

    public ProcessEDD() {
        super(ProcessRule.PART_ACTION_EDD, "EDD规则:工件交货期越早越优先");
        // TODO Auto-generated constructor stub
    }
    @Override
    public int compare(Process operTask1, Process operTask2) {
        if (operTask1.equals(operTask2))
            return 0;
        int i = -1;
        try {
            if (operTask1.getProduct().getDueDate() > operTask2.getProduct().getDueDate())
                i = 1;
            else if (operTask1.getProduct().getDueDate() - operTask2.getProduct().getDueDate() == 0) {
                if (operTask1.getWorkTime() > operTask2.getWorkTime())
                    i = 1;
                else if (operTask1.getWorkTime() == operTask2.getWorkTime()) {
                    if (operTask1.getID() > operTask2.getID())
                        i = 1;
                }
            }
        } catch (RuntimeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return i;
    }
}
