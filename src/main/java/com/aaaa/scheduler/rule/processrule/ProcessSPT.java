package com.aaaa.scheduler.rule.processrule;

import com.aaaa.scheduler.pojo.Process;

import java.util.Comparator;

/**
 * SPT规则，工时越短越优先
 *
 */
public class ProcessSPT extends ProcessRule implements Comparator<Process> {

    public ProcessSPT() {
        super(ProcessRule.PART_ACTION_SPT, "SPT规则:工时越短越优先");
        // TODO Auto-generated constructor stub
    }
    @Override
    public int compare(Process operTask1, Process operTask2) {
        if (operTask1.equals(operTask2))
            return 0;
        int i = -1;
        try {
            if (operTask1.getWorkTime() > operTask2.getWorkTime())
                i = 1;
            else if (operTask1.getWorkTime() - operTask2.getWorkTime() == 0) {
                if (operTask1.getRemainWorkTime() > operTask2.getRemainWorkTime())
                    i = 1;
                else if (operTask1.getRemainWorkTime() == operTask2.getRemainWorkTime()) {
                    // 交货期
                    if (operTask1.getID() > operTask2.getID())
                        i = 1;
                }
            }
        } catch (RuntimeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return i;
    }
}
