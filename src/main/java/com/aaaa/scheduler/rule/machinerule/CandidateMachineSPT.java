package com.aaaa.scheduler.rule.machinerule;



import com.aaaa.scheduler.pojo.CandidateMachine;

import java.util.Comparator;

/**
 * SPT规则，生产时间越短越优先
 *
 */
public class CandidateMachineSPT extends CandidateMachineRule implements Comparator<CandidateMachine> {

    public CandidateMachineSPT() {
        super(CandidateMachineRule.PART_ACTION_SPT, "SPT规则:总生产时间越短越优先");
        // TODO Auto-generated constructor stub
    }
    @Override
    public int compare(CandidateMachine canMachine1, CandidateMachine canMachine2) {
        if (canMachine1.equals(canMachine2))
            return 0;
        int i = -1;
        try {
            if (canMachine1.getDuration() > canMachine2.getDuration())
                i = 1;
            else if (canMachine1.getDuration() - canMachine2.getDuration() == 0) {
                if (canMachine1.getRunTime() > canMachine2.getRunTime())
                    i = 1;
                else if (canMachine1.getRunTime() == canMachine2.getRunTime()) {
                    // 交货期
                    if (canMachine1.getMachine().getID() > canMachine2.getMachine().getID())
                        i = 1;
                }
            }
        } catch (RuntimeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return i;
    }
}
