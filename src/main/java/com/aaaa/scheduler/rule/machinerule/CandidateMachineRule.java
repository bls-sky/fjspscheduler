package com.aaaa.scheduler.rule.machinerule;


import com.aaaa.scheduler.pojo.CandidateMachine;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Comparator;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CandidateMachineRule implements Comparator<CandidateMachine>, Serializable {

    /**
     * 规则序号
     */
    public int ruleID;
    /**
     * 规则名称
     */
    public String ruleName;

    /**
     * SPT规则，工时越短越优先
     */
    public static final int PART_ACTION_SPT = 1;

    /**
     * SAT 已安排工时最少优先
     */
    public static final int PART_ACTION_SAT = 2;
    /*
     * 最少负荷
     */
    protected static final int PART_ACTION_SL = 3;
    /*
     * 最少利用率
     */



    @Override
    public int compare(CandidateMachine o1, CandidateMachine o2) {
        return 0;
    }
}
