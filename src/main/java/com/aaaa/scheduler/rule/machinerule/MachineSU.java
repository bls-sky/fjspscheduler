package com.aaaa.scheduler.rule.machinerule;



import com.aaaa.scheduler.pojo.CandidateMachine;
import com.aaaa.scheduler.pojo.Machine;

import java.util.Comparator;

/**
 * SU：最小利用率优先
 *
 */
public class MachineSU extends CandidateMachineRule implements Comparator<CandidateMachine> {

    public MachineSU() {
        super(CandidateMachineRule.PART_ACTION_SL, "SU：最小利用率优先");
        // TODO Auto-generated constructor stub
    }
    @Override
    public int compare(CandidateMachine canmachine1, CandidateMachine canmachine2) {
        Machine machine1 = canmachine1.getMachine(), machine2 = canmachine2.getMachine();
        if (machine1.equals(machine2))
            return 0;
        int i = -1;
        try {
            if (machine1.getUtilRation() > machine2.getUtilRation())
                i = 1;
            else if (machine1.getUtilRation() == machine2.getUtilRation()) {
                if (machine1.getID() > machine2.getID())
                    i = 1;
            }
        } catch (RuntimeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return i;
    }
}
